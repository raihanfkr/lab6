from django.shortcuts import render, redirect
from .models import Status
from . import forms

# Create your views here.
def landing(request):
    form = forms.StatusForm(request.POST) 
    if request.method == 'POST' and form.is_valid():
        status = form.save(commit=False)
        status.save()
        return redirect('landing')

    else:
        form = forms.StatusForm()
    stats = Status.objects.all()
    return render(request, 'lab6app/landing.html', {'stats': stats, 'form': form})
# def status(request):
    


    