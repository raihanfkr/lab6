from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import landing
from .models import Status
from .forms import StatusForm
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Lab6UnitTest(TestCase):
    def test_lab6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

    def test_lab6_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'lab6app/landing.html')

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_lab6_models(self):
        data = Status(status = 'Ini status')
        data.save()
        response = Client().get('/')    
        self.assertContains(response, 'Ini status')

    def test_lab6_add_status(self):
        data = {"status" : "Nama saya Raihan"}
        response = Client().post('/', data)
        self.assertEqual(response.status_code, 302)

    def test_lab6_status_form(self):
        data = {'status' : 'Nama saya Raihan'}
        form = StatusForm(data=data)
        self.assertTrue(form.is_valid())

    # def test_lab6_text_is_exist(self):
    #     response = Client().get('/')
    #     self.assertContains(response, "Nama saya Raihan")

    # def test_landing_page_content_is_written(self):
    #     #Content can not be null
    #     self.assertIsNone(landing_page_content)

    #     #Content is filled with max 300 characters
    #     self.assertTrue(len(landing_page_content) <= 300)

    # def test_landing_page_is_completed(self):
    #     request = HttpRequest()
    #     response = index(request)
    #     html_response = response.content.decode('utf8')


    # def test_model_can_create_new_status(self):
    #     # Creating a new status
    #     new_status = Status.objects.create(status='Aku lagi males banget hari ini hhh')

    #     # Retrieving all available activity
    #     counting_all_available_status = Status.objects.all().count()
    #     self.assertEqual(counting_all_available_status, 1)

    # def test_status_form_input_has_placeholder_and_css_classes(self):
    #     form = StatusForm()
    #     self.assertIn('class="form-group col-md-10 offset-md-1 text-center"', form.as_p())
    #     self.assertIn('id="id_status"', form.as_p())

    # def test_lab6_post_success_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/landing/', {'status': 'Aku ingin pergi'})
    #     self.assertEqual(response_post.status_code, 302)

    #     response= Client().get('/landing/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(test, html_response)

    # def test_lab6_post_error_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/status/', {'status': ''})
    #     self.assertEqual(response_post.status_code, 302)

    #     response= Client().get('/status/')
    #     html_response = response.content.decode('utf8')
    #     self.assertNotIn(test, html_response)


class Lab6FunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.refresh()
        self.browser.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input(self):
        self.browser.get(self.live_server_url)
        status = self.browser.find_element_by_id('id_status')
        status.send_keys("Coba coba")
        submit = self.browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)
        #time.sleep(3)
        self.browser.get(self.live_server_url)
        #time.sleep(3)
        self.assertIn('Coba coba', self.browser.page_source)

    # def test_input(self):
    #     self.browser.get(self.live_server_url)
    #     status = self.browser.find_element_by_id('id_status')
    #     status.send_keys("Coba coba")
    #     time.sleep(3)
    #     submit = self.browser.find_element_by_id('submit')
    #     submit.send_keys(Keys.RETURN)
    #     time.sleep(3)
    #     self.browser.get(self.live_server_url)
    #     time.sleep(3)
    #     self.assertIn('Coba coba', self.browser.page_source)


# def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story6FuntionalTest, self).setUp()


   

#     def setUp(self):
#         chrome_options = Options()
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Lab6FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab6FunctionalTest, self).tearDown()

    # def test_input_todo(self):
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get('http://127.0.0.1:8000/landing/')
    #     # find the form element
    #     status = selenium.find_element_by_id('id_status')

    #     submit = selenium.find_element_by_id('submit')

    #     # Fill the form with data
    #     status.send_keys('Mengerjakan Lab PPW')

    #     # submitting the form
    #     submit.send_keys(Keys.RETURN)

# # Create your tests here.
